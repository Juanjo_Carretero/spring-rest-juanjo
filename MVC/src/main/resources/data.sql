DROP table IF EXISTS empleado;

create table empleado(
	id int auto_increment,
	nombre varchar(250),
	apellido varchar(250),
    trabajo enum('camarero','informatico','repartidor'),
    salario decimal(5)
);

insert into empleado (nombre, apellido, trabajo, salario)values('Manuel','Martinez','camarero',1500.0);
insert into empleado (nombre, apellido, trabajo, salario)values('Hugo','Garcia','informatico', 2000.0);
insert into empleado (nombre, apellido, trabajo, salario)values('David','Lopez', 'repartidor', 1200.0);